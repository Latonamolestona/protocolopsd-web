¿Por qué los respositorios son públicos y privados?
Lo que esto signiFica es que: 
#Privados# - Repositorios donde el usuario creador permite el acceso 
únicamente a él mismo, o a la personas con las que quiera
compartir el trabajo.
#Públicos# - Repositorios donde el usuario da acceso libre a las
personas para modificar el mismo, o dar apoyo.

¿Por qué un repositorio público es visible y un repositorio privado no es visible?
#H0# - La configuración predeterminada de la página asocia la palabra *Público* 
     con el acceso al repositorio a cualquier personaa que deseé verlo.
#H1# - La configuración predeterminada de la página asocia la palabra *Privado* 
     con el acceso restringido al propietario o a las personas que él quiera compartirlo.
