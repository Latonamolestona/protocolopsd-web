## README ##

# Protocolo PSD #

_Ésta investigación inició gracias al acercamiento que tuve a un miebro de la familia González, en la cual uno de sus integrantes cuenta con una persona con Síndrome de Down que tuve oportunidad de conocer. El fruto de interés por ahondar en el tema se me presentó al escuchar la motivación e iniciativa que el Sr. González hablaba sobre la condición de su hijo, sobre el acercamiento de la perspectiva de la persona con trisomía 21 hacia los demás. Ésta inquietud me llevó a profundizar y revisar diversos artículos donde la persona con Síndrome de Down fuera protagonista y donde ciertamente los menos hablaban desde su experiencia y los más sobre descripción y alteraciones del síndrome. En este sentido, me pareció importante rescatar el estudio de la percepción corporal en personas con Síndrome de Down, indagando así su perspectiva corporal en la ciudad de México._

### Lista de herramientas ###

* Html 5
* Css 3
* Javascript es6
